from MyParser import *


TEST_STRINGS = [ 'SELECT C1,C2 FROM T1 WHERE C1=5.23 AND C2>3',
				 'SELECT C1,C2,C3 FROM T1',
				 'SELECT C1 FROM T1,T2,T3',
				 'SELECT C1 FROM T1 WHERE S1>5',
				 'SELECT C1 FROM T1 WHERE S1>5 AND S2<C2',
				 'SELECT C1 FROM T1 WHERE S1>5 AND S2<C2 AND S3=C3'
				 ]
# TEST_STRINGS = [
# 	'SELECT C1,C2 SELECT T1 FROM C1<5.23 AND c2 = 5'
#
# ]
for testCase in TEST_STRINGS :
	lex = Lexer ( testCase + '$' )
	print testCase

	tk = lex.nextToken ( )
	while (tk.getTokenCategory ( ) != EOI) :
		print tk
		tk = lex.nextToken ( )
	print '======================================'

	parser = Parser ( testCase )
	parser.run ( )
	print('\n\n\n\n')

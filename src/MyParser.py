'''

@author:
	Namw: Sultan Mahmud Sajal
	Email: sxs2561@psu.edu

	Description of the file:
	This file contains mainly the 3 classes: Token, Lexer and Parser



'''

import sys


INT, FLOAT, ID, COMMA, EQUAL, GREATERTHAN, LESSTHAN, OPERATOR, AND, KEYWORD, EOI, INVALID = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
DIGITS = "0123456789"

'''
	This methods converts the type of the token to string
'''


def typeToString ( tp ) :
	if (tp == INT) :
		return "Int"
	elif (tp == FLOAT) :
		return "Float"
	elif (tp == KEYWORD) :
		return "Keyword"
	elif (tp == ID) :
		return "ID"
	elif (tp == COMMA) :
		return "Comma"
	elif (tp in [ EQUAL, GREATERTHAN, LESSTHAN ]) :
		return "Operator"
	elif (tp == EOI) :
		return "EOI"
	else :
		return "Invalid"


'''
	This is the Token class which is the wrapper for token
'''


class Token :

	def __init__ ( self, tokenCategory, tokenValue ) :

		self.tokenCategory = tokenCategory
		self.tokenValue = tokenValue


	def getTokenCategory ( self ) :
		return self.tokenCategory


	def getTokenValue ( self ) :
		return self.tokenValue


	def __repr__ ( self ) :
		if (self.tokenCategory in [ INT, FLOAT, ID ]) :
			return self.tokenValue
		elif (self.tokenCategory == COMMA) :
			return ","
		elif (self.tokenCategory == EQUAL) :
			return "="
		elif (self.tokenCategory == LESSTHAN) :
			return "<"
		elif (self.tokenCategory == GREATERTHAN) :
			return ">"
		elif (self.tokenCategory == KEYWORD) :
			return self.tokenValue
		elif (self.tokenCategory == EOI) :
			return ""
		else :
			return "invalid"


'''
	This is the Lexer class
'''


class Lexer :

	def __init__ ( self, s ) :
		self.statement = s
		self.index = 0
		self.nextChar ( )


	'''
		Method for generating the next token
	'''


	def nextToken ( self ) :

		while True :
			if self.ch.isalpha ( ) :
				id = self.consumeChar ( LETTERS + DIGITS )
				if id in [ "SELECT", "FROM", "WHERE", "AND" ] :
					return Token ( KEYWORD, id )
				else :
					return Token ( ID, id )
			elif self.ch.isdigit ( ) :
				num = self.consumeChar ( DIGITS )
				if self.ch != "." :
					return Token ( INT, num )
				num += self.ch
				self.nextChar ( )
				if self.ch.isdigit ( ) :
					num += self.consumeChar ( DIGITS )
					return Token ( FLOAT, num )
				else :
					return Token ( INVALID, num )
			elif self.ch == ' ' :
				self.nextChar ( )
			elif self.ch == ',' :
				self.nextChar ( )
				return Token ( COMMA, ',' )
			elif self.ch == '=' :
				self.nextChar ( )
				return Token ( EQUAL, '=' )
			elif self.ch == '<' :
				self.nextChar ( )
				return Token ( LESSTHAN, '<' )
			elif self.ch == '>' :
				self.nextChar ( )
				return Token ( GREATERTHAN, '>' )
			elif self.ch == "$" :
				return Token ( EOI, "" )
			else :
				self.nextChar ( )
				return Token ( INVALID, self.ch )


	'''
		method for getting the next character
	'''


	def nextChar ( self ) :
		self.ch = self.statement [ self.index ]
		self.index = self.index + 1
		return


	'''
		consumes the character
	'''


	def consumeChar ( self, charSet ) :
		r = self.ch
		self.nextChar ( )
		while (self.ch in charSet) :
			r = r + self.ch
			self.nextChar ( )
		return r


	'''
		checks equality of the character
	'''


	def checkChar ( self, c ) :
		self.nextChar ( )
		if (self.ch == c) :
			self.nextChar ( )
			return True
		else :
			return False


'''
	This is the Parser Class
'''


class Parser :

	def __init__ ( self, s ) :
		self.lexer = Lexer ( s + '$' )
		self.token = self.lexer.nextToken ( )


	'''
		highest level code running the parser 
	'''


	def run ( self ) :
		self.query ( )
		return


	'''
		highest level of recursive parsing
	'''


	def query ( self ) :
		print "<Query>"
		val = self.match ( KEYWORD )
		if val == "SELECT" :
			print "\t<Keyword>SELECT</Keyword>"
		else :
			self.error2 ( tp = val, expected = "SELECT" )
		self.idList ( )
		val = self.match ( KEYWORD )
		if val == "FROM" :
			print "\t<Keyword>FROM</Keyword>"
		else :
			self.error2 ( tp = val, expected = "FROM" )
		self.idList ( )
		while self.token.getTokenCategory ( ) == KEYWORD :
			val = self.match ( KEYWORD )
			if val == "WHERE" :
				print "\t<Keyword>WHERE</Keyword>"
				self.condList ( )
			else :
				self.error2 ( tp = val, expected = "WHERE" )

		self.match ( EOI )
		print "</Query>"

		return


	'''
		parsing method for IdList
	'''


	def idList ( self ) :
		print "\t<IdList>"
		val = self.match ( ID )
		print "\t\t<Id>" + val + "</Id>"
		while self.token.getTokenCategory ( ) == COMMA :
			print "\t\t<Comma>,</Comma>"
			self.token = self.lexer.nextToken ( )
			val = self.match ( ID )
			print "\t\t<Id>" + val + "</Id>"
		print "\t</IdList>"
		return


	'''
		parsing method for CondList
	'''


	def condList ( self ) :
		print "\t<CondList>"
		self.cond ( )

		while self.token.getTokenCategory ( ) == KEYWORD and self.token.getTokenValue ( ) == "AND" :
			print "\t\t<Keyword>AND</Keyword>"
			self.token = self.lexer.nextToken ( )
			self.cond ( )
		print "\t</CondList>"
		return


	'''
		Parsing method for Cond
	'''


	def cond ( self ) :
		print "\t\t<Cond>"
		val = self.match ( ID )
		print "\t\t\t<Id>" + val + "</Id>"

		val = self.match ( [ EQUAL, GREATERTHAN, LESSTHAN ] )

		print "\t\t\t<Operator>" + val + "</Operator>"
		self.term ( )
		print "\t\t</Cond>"

		return


	'''
		parsing method for term
	'''


	def term ( self ) :
		if self.token.getTokenCategory ( ) == ID :
			print "\t\t\t<Id>" + self.token.getTokenValue ( ) + "</Id>"
		elif self.token.getTokenCategory ( ) == INT :
			print "\t\t\t<Int>" + self.token.getTokenValue ( ) + "<\Int>"
		elif self.token.getTokenCategory ( ) == FLOAT :
			print "\t\t\t<Float>" + self.token.getTokenValue ( ) + "<\Float>"
		else :
			print "Syntax Error: expecting an ID, an Int, or a Float; saw: " + typeToString (
					self.token.getTokenCategory ( ) )
			sys.exit ( 1 )
		self.token = self.lexer.nextToken ( )
		return


	'''
		method for matching
	'''


	def match ( self, tp ) :
		val = self.token.getTokenValue ( )
		if type ( tp ) == list :
			for t in tp :
				if (self.token.getTokenCategory ( ) == t) :
					self.token = self.lexer.nextToken ( )
					return val
			self.error ( tp )
		else :
			if (self.token.getTokenCategory ( ) == tp) :
				self.token = self.lexer.nextToken ( )
			else :
				self.error ( tp )
			return val


	'''
		error generating function-1
	'''


	def error ( self, tp ) :

		print "Syntax error: expecting: " + str ( tp ) + " ; saw: " + str ( self.token.getTokenValue ( ) )
		sys.exit ( 1 )


	'''
		error generating function-2
	'''


	def error2 ( self, tp, expected ) :
		print "Syntax Error: expecting: " + str ( expected ) + "  ;saw: " + str ( tp )
		sys.exit ( 1 )
